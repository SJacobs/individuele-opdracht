/*
 *	Create script ontwikkelopdracht Bol.com
 *	Simin Jacobs
 */
 
--Disable prompts when using &
SET DEFINE OFF

--Drop all tables and constraints
DROP TABLE Gebruiker CASCADE CONSTRAINTS;
DROP TABLE Categorie CASCADE CONSTRAINTS;
DROP TABLE Product CASCADE CONSTRAINTS;
DROP TABLE Factuur CASCADE CONSTRAINTS;
DROP TABLE Bestelregel CASCADE CONSTRAINTS;
DROP TABLE Bestelling CASCADE CONSTRAINTS;
DROP TABLE BekekenProduct CASCADE CONSTRAINTS;
DROP TABLE VerlangLijstProduct CASCADE CONSTRAINTS;
DROP TABLE Review CASCADE CONSTRAINTS;
DROP TABLE Aanbieding CASCADE CONSTRAINTS;
DROP TABLE Nieuwsbrief CASCADE CONSTRAINTS;
DROP TABLE GebruikerNieuwsbrief CASCADE CONSTRAINTS;
DROP TABLE Adres CASCADE CONSTRAINTS;
DROP TABLE GebruikerAdres CASCADE CONSTRAINTS;
DROP TABLE Foto CASCADE CONSTRAINTS;
DROP TABLE WinkelwagenProduct CASCADE CONSTRAINTS;

--Drop all sequences
DROP SEQUENCE SEQ_Gebruiker;
DROP SEQUENCE SEQ_Categorie;
DROP SEQUENCE SEQ_Product;
DROP SEQUENCE SEQ_Bestelling;
DROP SEQUENCE SEQ_Bestelregel;
DROP SEQUENCE SEQ_Nieuwsbrief;
DROP SEQUENCE SEQ_Factuur;
DROP SEQUENCE SEQ_Foto;

--Create tables
CREATE TABLE Gebruiker
(
	id					NUMBER(10) PRIMARY KEY,
	naam				VARCHAR(255),
	wachtwoord			VARCHAR(255) NOT NULL,
	geboortedatum		DATE,
	telefoonnummer		VARCHAR(255),
	email				VARCHAR(255) NOT NULL UNIQUE,
	admin				SMALLINT DEFAULT 0,
	CONSTRAINT CHK_LengteWachtwoord CHECK(LENGTH(wachtwoord) BETWEEN 6 AND 20)
);

CREATE TABLE Categorie
(
	id					NUMBER(10) PRIMARY KEY,
	Categorie_id		NUMBER(10),
	naam				VARCHAR(255) NOT NULL,
	CONSTRAINT FK_Categorie_Categorie_id FOREIGN KEY(Categorie_id)REFERENCES Categorie(id)
);

CREATE TABLE Product
(
	id					NUMBER(10) PRIMARY KEY,
	Categorie_id		NUMBER(10) NOT NULL,
	naam				VARCHAR(255) NOT NULL,
	prijs				NUMBER(10,2) NOT NULL,
	merk				VARCHAR(255),
	type				VARCHAR(255),
	gebruikt			SMALLINT NOT NULL,
	eigenschappen		VARCHAR(255) NOT NULL,
	beschrijving		VARCHAR(2000) NOT NULL,
	CONSTRAINT FK_Product_Categorie_id FOREIGN KEY(Categorie_id)REFERENCES Categorie(id),
	CONSTRAINT CHK_Price CHECK(prijs > 0)
);

CREATE TABLE WinkelwagenProduct
(
	Gebruiker_id		NUMBER(10),
	Product_id			NUMBER(10),
	aantal				NUMBER(10),
	CONSTRAINT FK_WProduct_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id),
	CONSTRAINT FK_WProduct_Product_id FOREIGN KEY(Product_id)REFERENCES Product(id),
	CONSTRAINT PK_WinkelwagenProduct PRIMARY KEY(Gebruiker_id, Product_id)
);

CREATE TABLE VerlangLijstProduct
(
	Gebruiker_id		NUMBER(10),
	Product_id			NUMBER(10),
	aantal				NUMBER(10),
	CONSTRAINT FK_VProduct_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id),
	CONSTRAINT FK_VProduct_Product_id FOREIGN KEY(Product_id)REFERENCES Product(id),
	CONSTRAINT PK_VerlanglijstProduct PRIMARY KEY(Gebruiker_id, Product_id)
);

CREATE TABLE Factuur
(
    id					NUMBER(10) PRIMARY KEY,
    Gebruiker_id		NUMBER(10) NOT NULL,
    datum				DATE NOT NULL,
    bedrag				NUMBER(10,2) NOT NULL,
    vervaldatum			DATE NOT NULL,
    openstaand			NUMBER(10,2) NOT NULL,
	betaald				SMALLINT NOT NULL,
    CONSTRAINT FK_Factuur_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id),
	CONSTRAINT CHK_Bedrag CHECK(bedrag > 0),
	CONSTRAINT CHK_Openstaand CHECK(openstaand >= 0)
);

CREATE TABLE Bestelling
(
	id					NUMBER(10) PRIMARY KEY,
	Gebruiker_id		NUMBER(10) NOT NULL,
	datum				DATE NOT NULL,
	CONSTRAINT FK_Bestelling_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id)
);

CREATE TABLE Bestelregel
(
	id					NUMBER(10) PRIMARY KEY,
	Product_id			NUMBER(10) NOT NULL,
	Bestelling_id		NUMBER(10) NOT NULL,
	aantal				NUMBER(10) NOT NULL,
	CONSTRAINT FK_Bestelregel_Product_id FOREIGN KEY(Product_id)REFERENCES Product(id),
	CONSTRAINT FK_Bestelregel_Bestelling_id FOREIGN KEY(Bestelling_id)REFERENCES Bestelling(id)
);

CREATE TABLE BekekenProduct
(
	Product_id			NUMBER(10) NOT NULL,
	Gebruiker_id		NUMBER(10) NOT NULL,
	datum				DATE NOT NULL,
	CONSTRAINT FK_BekekenProduct_Product_id FOREIGN KEY(Product_id)REFERENCES Product(id),
	CONSTRAINT FK_BekekenProduct_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id),
	CONSTRAINT PK_BekekenProduct PRIMARY KEY(Product_id, Gebruiker_id, datum)
);
  
CREATE TABLE Review
(
	Gebruiker_id		NUMBER(10) NOT NULL,
	Product_id			NUMBER(10) NOT NULL,
	datum				DATE NOT NULL,
	review				VARCHAR(255) NOT NULL,
	rating				NUMBER(1),
	CONSTRAINT FK_Review_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id),
	CONSTRAINT FK_Review_Product_id FOREIGN KEY(Product_id)REFERENCES Product(id),
	CONSTRAINT PK_Review PRIMARY KEY(Gebruiker_id, Product_id, datum),
	CONSTRAINT CHK_rating CHECK(rating BETWEEN 1 AND 5)
);

CREATE TABLE Aanbieding
(
	Product_id			NUMBER(10) NOT NULL,
	datumBegin			DATE NOT NULL,
	datumEinde			DATE NOT NULL,
	prijs				NUMBER(10) NOT NULL,
	CONSTRAINT FK_Aanbieding_Product_id FOREIGN KEY(Product_id)REFERENCES Product(id),
	CONSTRAINT PK_Aanbieding PRIMARY KEY(Product_id, datumBegin, datumEinde)
);

CREATE TABLE Nieuwsbrief
(
	id					NUMBER(10) PRIMARY KEY,
	naam				VARCHAR(255) NOT NULL,
	beschrijving		VARCHAR(255)
);

CREATE TABLE GebruikerNieuwsbrief
(
	Gebruiker_id		NUMBER(10) NOT NULL,
	Nieuwsbrief_id		NUMBER(10) NOT NULL,
	datumStart			DATE NOT NULL,
	CONSTRAINT FK_GebNieuwsbr_Gebruiker_id FOREIGN KEY(Gebruiker_id)REFERENCES Gebruiker(id),
	CONSTRAINT FK_GebNieuwsbr_Nieuwsbrief_id FOREIGN KEY(Nieuwsbrief_id)REFERENCES Nieuwsbrief(id),
	CONSTRAINT PK_GebruikerNieuwsbrief PRIMARY KEY(Gebruiker_id, Nieuwsbrief_id)
);

CREATE TABLE Adres
(
	postcode			VARCHAR(6) NOT NULL,
	huisnummer			VARCHAR(255) NOT NULL,
	naam				VARCHAR(255) NOT NULL,
	bedrijfsnaam		VARCHAR(255),
	CONSTRAINT PK_Adres PRIMARY KEY(postcode, huisnummer)
);

CREATE TABLE GebruikerAdres
(
	Gebruiker_id		NUMBER(10) NOT NULL,
	Adres_postcode		VARCHAR(6) NOT NULL,
	Adres_huisnummer	VARCHAR(255) NOT NULL,
	CONSTRAINT FK_GebAdres_Gebruiker_id FOREIGN KEY(Gebruiker_id) REFERENCES Gebruiker(id),
	CONSTRAINT FK_GebAdres_Adres FOREIGN KEY(Adres_postcode, Adres_huisnummer) REFERENCES Adres(postcode, huisnummer),
	CONSTRAINT PK_GebruikerAdres PRIMARY KEY(Gebruiker_id, Adres_postcode, Adres_huisnummer)
);

CREATE TABLE Foto
(
	id					NUMBER(10) PRIMARY KEY,
	Product_id			NUMBER(10) NOT NULL,
	url					VARCHAR(255) DEFAULT 'http://i.imgur.com/KUy5w1I.jpg',
	CONSTRAINT FK_Foto_Product FOREIGN KEY(Product_id) REFERENCES Product(id)
);

-- Auto increment for Gebruiker --

CREATE SEQUENCE SEQ_Gebruiker
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Gebruiker	
	before insert on Gebruiker
	for each row
	begin
	select SEQ_Gebruiker.nextval into :new.id from dual;
end;
/

-- Auto increment for Categorie --

CREATE SEQUENCE SEQ_Categorie
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Categorie	
	before insert on Categorie
	for each row
	begin
	select SEQ_Categorie.nextval into :new.id from dual;
end;
/

-- Auto increment for Product --

CREATE SEQUENCE SEQ_Product
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Product	
	before insert on Product
	for each row
	begin
	select SEQ_Product.nextval into :new.id from dual;
end;
/

-- Auto increment for Bestelregel --

CREATE SEQUENCE SEQ_Bestelregel
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Bestelregel	
	before insert on Bestelregel
	for each row
	begin
	select SEQ_Bestelregel.nextval into :new.id from dual;
end;
/

-- Auto increment for Bestelling --

CREATE SEQUENCE SEQ_Bestelling
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Bestelling	
	before insert on Bestelling
	for each row
	begin
	select SEQ_Bestelling.nextval into :new.id from dual;
end;
/

-- Auto increment for Nieuwsbrief --

CREATE SEQUENCE SEQ_Nieuwsbrief
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Nieuwsbrief
	before insert on Nieuwsbrief
	for each row
	begin
	select SEQ_Nieuwsbrief.nextval into :new.id from dual;
end;
/

-- Auto increment for Factuur --

CREATE SEQUENCE SEQ_Factuur
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Factuur
	before insert on Factuur
	for each row
	begin
	select SEQ_Factuur.nextval into :new.id from dual;
end;
/

-- Auto increment for Foto --

CREATE SEQUENCE SEQ_Foto
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Foto
	before insert on Foto
	for each row
	begin
	select SEQ_Foto.nextval into :new.id from dual;
end;
/

-- Insert test Gebruikers --

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Henk Houtzagers', 
	'Henk123', 
	TO_DATE('03/05/1988 13:50:02', 'dd/mm/yyyy hh24:mi:ss'), 
	'0657907372', 
	'henk_1988@hotmail.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Bert Braun', 
	'bert654', 
	TO_DATE('03/05/1973 19:25:32', 'dd/mm/yyyy hh24:mi:ss'), 
	'0627903351', 
	'bertbraun@gmail.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Jan Jansen', 
	'jan890', 
	TO_DATE('01/09/1989 20:35:12', 'dd/mm/yyyy hh24:mi:ss'), 
	'0621903250', 
	'jjansen@gmail.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Kees Kroket', 
	'kees05', 
	TO_DATE('21/10/1992 02:55:50', 'dd/mm/yyyy hh24:mi:ss'), 
	'0622913259', 
	'keeskr@gmail.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Peter Pan', 
	'panda1', 
	TO_DATE('29/11/1996 22:10:58', 'dd/mm/yyyy hh24:mi:ss'), 
	'0625361298', 
	'peterpanda@gmail.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Simin Jacobs', 
	'Simin123', 
	TO_DATE('18/03/1996 19:02:02', 'dd/mm/yyyy hh24:mi:ss'), 
	'0653907378', 
	'simin@jacobs.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Jan Oonk', 
	'jano0nk', 
	TO_DATE('18/03/1982 10:02:02', 'dd/mm/yyyy hh24:mi:ss'), 
	'0653437578', 
	'janoonk@fontys.nl'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Bob Bam', 
	'bambob', 
	TO_DATE('12/10/1992 10:12:02', 'dd/mm/yyyy hh24:mi:ss'), 
	'0653431570', 
	'bab@ziggo.nl'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Anna Aang', 
	'aaa123', 
	TO_DATE('05/10/1992 10:12:02', 'dd/mm/yyyy hh24:mi:ss'), 
	'0603431570', 
	'aaang@hotmail.com'
);

INSERT INTO Gebruiker(naam, wachtwoord, geboortedatum, telefoonnummer, email)
values(
	'Bob Poels', 
	'bobp555', 
	TO_DATE('05/12/1962 15:50:59', 'dd/mm/yyyy hh24:mi:ss'), 
	'0601131570', 
	'bobp@live.nl'
);

INSERT INTO Gebruiker(wachtwoord, email)
values(
	'Test123', 
	'simin'
);

INSERT INTO Gebruiker(wachtwoord, email, admin)
values(
	'Test123', 
	'admin',
	1
);

-- Insert test Categorieen --

INSERT INTO Categorie(naam)
values(
	'Muziek, Film & Games'
);

INSERT INTO Categorie(naam, Categorie_id)
values(
	'Games',
	(SELECT id
	FROM Categorie
	WHERE naam = 'Muziek, Film & Games')
);

INSERT INTO Categorie(naam, Categorie_id)
values(
	'Muziek',
	(SELECT id
	FROM Categorie
	WHERE naam = 'Muziek, Film & Games')
);

INSERT INTO Categorie(naam, Categorie_id)
values(
	'Muziek-cd''s & lp''s',
	(SELECT id
	FROM Categorie
	WHERE naam = 'Muziek')
);

INSERT INTO Categorie(naam, Categorie_id)
values(
	'PC gaming',
	(SELECT id
	FROM Categorie
	WHERE naam = 'Games')
);

INSERT INTO Categorie(naam)
values(
	'Computer'
);

INSERT INTO Categorie(naam, Categorie_id)
values(
	'Laptops',
	(SELECT id
	FROM Categorie
	WHERE naam = 'Computer')
);

-- Insert test Producten --

INSERT INTO Product(Categorie_id, naam, prijs, merk, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'PC gaming'),
	'FIFA 16 - PC',
	29.99,
	'Electronic Arts',
	0,
	'Platform: PC',
	'Voetbal draait om het moment... met FIFA 2016 maak jij dat moment speciaal. FIFA 16 innoveert op alle vlakken om een gebalanceerde, authentieke en spannende voetbalervaring te bieden die je laat spelen op jouw manier en in jouw favoriete speltypen. Met innovatieve gameplaykenmerken brengt FIFA 16 Verdedigen met zelfvertrouwen, Controle op het middenveld en biedt je de gereedschappen om meer Magische momenten te creëren dan ooit.'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'PC gaming'),
	'Need For Speed 2015 - PC',
	49.99,
	'Electronic Arts',
	0,
	'Platform: PC',
	'Need For Speed 2015. Race in het donker door een enorme open wereld die is gebaseerd op de rauwe cultuur van het straatracen en beleef een totaal vernieuwde versie van Need for Speed, waarin je op vijf manieren kunt spelen. Kies je eigen pad in meerdere overlappende verhaallijnen, werk aan je reputatie en word de ultieme racelegende. Bij de ontwikkeling van Need for Speed stonden de feedback en wensen van de fans centraal. In deze game kun je je auto''s volledig aanpassen, rij je door een realistische open wereld met een authentieke straatracecultuur, en beleef je een meeslepend verhaal. De game speelt zich af in Ventura Bay. Je rijdt in het donker over slingerende heuvelwegen met uitzicht op de stad en scheurt door de scherpe bochten van stadswijken, waar de politie altijd op de loer ligt. Vijf echte autolegenden wachten op je. Laat je inspireren en uitdagen door deze helden van de hedendaagse autocultuur en verken de grenzen van je vaardigheden. Werk aan je reputatie en verdien respect om je einddoel te bereiken. Alleen de beste racer word de ultieme racelegende. '
);

INSERT INTO Product(Categorie_id, naam, prijs, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Muziek-cd''s & lp''s'),
	'Embrace',
	11.99,
	0,
	'Drager: CD, Artiest: Armin van Buuren',
	'Embrace is het nieuwste album van één van de meest succesvolle dj''s van de wereld: Armin van Buuren. Hij behaalde al een Dance Smash met de single en albumtrack ‘Strong Ones’ en is alarmschijf met track 6 van het album; ‘Heading Up High’ feat. Kensington. Ook de hit ‘Another You’ feat. Mr. Probz is inmiddels meer dan veertig miljoen keer gestreamd. De videoclip van de single is daarnaast meer dan vijf miljoen keer bekeken op YouTube. Armin van Buuren: "Na 76, Shivers, Imagine, Mirage en Intense kwam ik voor de titel van mijn nieuwe album weer uit op één woord. Embrace staat voor het omarmen van verschillende soorten instrumenten en geluiden als toevoeging op mijn huidige sound. Ik hoop dat mijn fans ook dit nieuwe hoofdstuk in mijn carrière zullen toejuichen, zoals ze tot nu toe altijd al gedaan hebben. Daarnaast ben ik ontzettend blij met de foto''s van Anton. Het was een eer om met hem te mogen werken en ik ben trots op het resultaat!". Anton Corbijn: "Ik heb Armin gefotografeerd met het idee om de indruk die het publiek van hem en zijn creaties heeft ietwat te veranderen. Het is zo''n aardige gozer, dus ik probeerde hem visueel juist een wat donkerdere kant op te sturen door een stel beelden te maken met een mengsel van referenties aan Sergeant Pepper en Mad Max. We waren gewoon speels bezig. Dat leek mij de juiste toon voor een veelzijdige dj die gek is op muziek en de hele wereld rondreist".'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Laptops'),
	'MSI GS70 6QD-011NL - Gaming Laptop',
	1689.00,
	'MSI',
	'GS70 6QD-011NL',
	0,
	'Kleur: Zwart',
	'De GS70 notebook van MSI is een snelle gaming-notebook en bedoeld voor de gamer die graag in stijl wil gamen. De GS70 is op zijn dikste punt maar 22mm dik. Dankzij de laatste Intel® Core™ i7-6700HQ processor, 8 GB werkgeheugen en een NVMe SSD van 256 GB kan deze notebook alles aan.'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Laptops'),
	'Asus X550ZE-DM159T - Laptop',
	599.00,
	'Asus',
	'X550ZE-DM159T',
	0,
	'Kleur: Zilver',
	'De Asus X550ZE is uitgerust met een AMD A10 quadcore processor, 12 GB werkgeheugen en een AMD Radeon R5 M230 videokaart. Deze krachtige onderdelen maken deze laptop geschikt voor zware rekentaken, foto- en videobewerking en het snel werken met al jouw programma''s.'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Laptops'),
	'Lenovo G50-80 - Laptop',
	529.00,
	'Lenovo',
	'G50-80',
	0,
	'Kleur: Zwart',
	'De Lenovo G50-80 is uitgerust met een Intel Core i5 processor, samen met 4 GB werkgeheugen. In jouw dagelijkse taken zorgt de Intel Core i5 processor voor een soepele werking. De harde schijf van 500 GB biedt voldoende opslag voor al jouw bestanden en programma''s. Bewaar video''s, fotoboeken en documenten op deze ruime harde schijf. De harde schijf heeft een extra cache-SSD van 8 GB. Deze zorgt er automatisch voor dat veelgebruikte bestanden en programma''s'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Laptops'),
	'Toshiba Satellite C55D-C-16F - Laptop',
	499.00,
	'Toshiba',
	'Satellite C55D-C-16F',
	0,
	'Kleur: Zwart',
	'De Toshiba Satellite C55D-C laptop is uitgerust met een AMD quadcore processor en een 256 GB SSD. De ingebouwde SSD zorgt ervoor dat Windows zeer snel opstart en programma''s vlot werken. De capaciteit van 256 GB is voldoende voor de opslag van veel bestanden. '
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Laptops'),
	'Microsoft Surface 3 - Hybride Laptop Tablet / 64 GB',
	449.00,
	'Microsoft',
	'Surface 3 - Hybride Laptop Tablet / 64 GB',
	0,
	'Kleur: Zilver',
	'De Surface 3 biedt jouw het beste van een tablet met de kracht van een laptop. Het apparaat is zeer licht en dun, doordat het van hoogwaardig magnesium is gebouwd. Zo kun je de Surface 3 gemakkelijk meenemen. Bovendien heeft dit 2-in-1 device van Microsoft een accuduur tot wel 10 uur, waardoor je het einde van de dag gemakkelijk haalt.'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'Laptops'),
	'HP 15-ac131nd - Laptop',
	499.00,
	'HP',
	'15-ac131nd - Laptop',
	0,
	'Kleur: Zilver',
	'Met het ontspiegeld Full HD-scherm, Intel Core i5 processor en DTS Studio Sound™ luidsprekers komt entertainment tot leven in de HP 15-ac131nd. De HP 15-ac131nd beschikt over een 500 GB harde schijf om al je foto''s, muziek en andere belangrijke bestanden op te slaan. Daarnaast is de laptop uitgevoerd met een CD/DVD Lezer/Brander, ingebouwde microfoon en webcam. Met HP 3D DriveGuard is je laptop ook onderweg beschermt.'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'PC gaming'),
	'Formula 1 - F1 2015 - PC',
	29.99,
	'Codemasters',
	0,
	'Platform: PC',
	'F1 2015 is gebaseerd op het Formule 1-seizoen van 2015 en bevat alle coureurs en racebanen die hierbij centraal staan. Je zult dus beroemdheden als Felipe Massa, Lewis Hamilton en Max Verstappen tegenkomen en kunt tegen hen racen op onder andere het Autódromo Hermanos Rodríguez. Bovendien bevat de game als bonus alle coureurs, auto''s en circuits uit het 2014-seizoen.'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'PC gaming'),
	'Assassin''s Creed: Unity - PC',
	19.99,
	'Ubisoft',
	0,
	'Platform: PC',
	'Parijs. Het jaar 1789. De Franse Revolutie verandert een prachtige stad in een plaats van terreur en chaos. De geplaveide straten worden overspoeld door het bloed van iedereen die durft op te staan tegen de onderdrukkende elite. Toch, terwijl Frankrijk lijdt, staat een jongeman genaamd Arno op om de ware krachten achter de Revolutie te onthullen. Deze achtervolging plaatst hem in het midden van een meedogenloze strijd en verandert hem in een echte Master Assassin.Dit is Assassin’s Creed® Unity, de next-gen evolutie van de blockbuster franchise, gevoed door de nieuwe Anvil Engine, opnieuw gemaakt voor een nieuwe generatie gaming. Van de bestorming van de Bastille tot de executie van koning Louis XVI. Ervaar de Franse Revolutie als nooit tevoren en help de inwoners van Frankrijk hun eigen lot te bepalen'
);

INSERT INTO Product(Categorie_id, naam, prijs, merk, gebruikt, eigenschappen, beschrijving)
values(
	(SELECT id
	FROM Categorie
	WHERE naam = 'PC gaming'),
	'Far Cry: Primal - PC',
	49.99,
	'Ubisoft',
	0,
	'Platform: PC',
	'De bekroonde Far Cry-reeks die de tropen en de Himalaya veroverde, richt zich nu op de oeroverlevingsstrijd van de mensheid met vernieuwende gameplay in een open wereld. Je krijgt te maken met gigantische beesten, adembenemende omgevingen en onvoorspelbare, brute confrontaties. Welkom in het stenen tijdperk, een tijd vol extreme gevaren en eindeloos veel avonturen. Gigantische mammoeten en sabeltandtijgers heersen op aarde en de mensheid staat onder aan de voedselketen. Als laatste overlevende van je jagersgroep leer je een dodelijk arsenaal te maken, meedogenloze roofdieren van je af te slaan en vijandelijke stammen te slim af te zijn in de strijd om het land Oros. Uiteindelijke sta jij aan de top van de voedselketen.'
);

-- Insert test Factuur --

INSERT INTO Factuur(Gebruiker_id, datum, bedrag, vervaldatum, openstaand, betaald)
values(
	1,
	TO_DATE('04/04/2016 21:29:49', 'dd/mm/yyyy hh24:mi:ss'), 
	10.50,
	TO_DATE('04/04/2017 21:29:49', 'dd/mm/yyyy hh24:mi:ss'), 
	0.00,
	1
);

INSERT INTO Factuur(Gebruiker_id, datum, bedrag, vervaldatum, openstaand, betaald)
values(
	2,
	TO_DATE('28/01/2016 20:30:38', 'dd/mm/yyyy hh24:mi:ss'), 
	49.99,
	TO_DATE('28/01/2017 20:30:38', 'dd/mm/yyyy hh24:mi:ss'), 
	49.99,
	0
);


INSERT INTO Factuur(Gebruiker_id, datum, bedrag, vervaldatum, openstaand, betaald)
values(
	3,
	TO_DATE('04/04/2016 17:39:49', 'dd/mm/yyyy hh24:mi:ss'), 
	29.50,
	TO_DATE('04/04/2017 17:39:49', 'dd/mm/yyyy hh24:mi:ss'), 
	0.00,
	1
);


INSERT INTO Factuur(Gebruiker_id, datum, bedrag, vervaldatum, openstaand, betaald)
values(
	4,
	TO_DATE('04/04/2016 11:35:49', 'dd/mm/yyyy hh24:mi:ss'), 
	89.50,
	TO_DATE('04/04/2017 11:35:49', 'dd/mm/yyyy hh24:mi:ss'), 
	0.00,
	1
);


INSERT INTO Factuur(Gebruiker_id, datum, bedrag, vervaldatum, openstaand, betaald)
values(
	4,
	TO_DATE('04/04/2016 03:29:49', 'dd/mm/yyyy hh24:mi:ss'), 
	75.00,
	TO_DATE('04/04/2017 03:29:49', 'dd/mm/yyyy hh24:mi:ss'), 
	75.00,
	0
);

-- Insert test Bestelling --

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	1,
	TO_DATE('01/03/2014 08:45:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	2,
	TO_DATE('01/03/2014 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	6,
	TO_DATE('01/03/2014 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	6,
	TO_DATE('10/01/2015 12:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	6,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	7,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	7,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	8,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	8,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	9,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	9,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO Bestelling(Gebruiker_id, datum)
values(
	10,
	TO_DATE('15/12/2016 13:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

-- Insert test Bestelregel --

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	1,
	1,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	1,
	5
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	3,
	2,
	3
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	1,
	2,
	6
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	2,
	10
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	1,
	3,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	3,
	1
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	4,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	4,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	5,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	2,
	5,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	4,
	6,
	8
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	4,
	5,
	2
);
	
INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	5,
	7,
	1
);
	
INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	6,
	8,
	1
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	7,
	9,
	1
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	8,
	9,
	1
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	9,
	10,
	2
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	8,
	11,
	12
);

INSERT INTO BestelRegel(Product_id, Bestelling_id, aantal)
values(
	9,
	11,
	12
);

-- Insert test BekekenProduct --

INSERT INTO BekekenProduct(Product_id, Gebruiker_id, datum)
values(
	1,
	1,
	TO_DATE('10/06/2013 18:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO BekekenProduct(Product_id, Gebruiker_id, datum)
values(
	1,
	2,
	TO_DATE('18/10/2013 23:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO BekekenProduct(Product_id, Gebruiker_id, datum)
values(
	2,
	2,
	TO_DATE('20/12/2013 15:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO BekekenProduct(Product_id, Gebruiker_id, datum)
values(
	2,
	3,
	TO_DATE('18/11/2013 10:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO BekekenProduct(Product_id, Gebruiker_id, datum)
values(
	2,
	4,
	TO_DATE('13/10/2013 20:59:49', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO BekekenProduct(Product_id, Gebruiker_id, datum)
values(
	2,
	6,
	TO_DATE('08/04/2016 16:57:01', 'dd/mm/yyyy hh24:mi:ss')
);

-- Insert test Aanbieding --

INSERT INTO Aanbieding(Product_id, datumBegin, datumEinde, prijs)
values(
	1,
	TO_DATE('05/04/2016 10:59:49', 'dd/mm/yyyy hh24:mi:ss'),
	TO_DATE('05/06/2016 11:59:49', 'dd/mm/yyyy hh24:mi:ss'),
	20
);

INSERT INTO Aanbieding(Product_id, datumBegin, datumEinde, prijs)
values(
	2,
	TO_DATE('05/04/2016 15:59:49', 'dd/mm/yyyy hh24:mi:ss'),
	TO_DATE('09/04/2016 15:59:49', 'dd/mm/yyyy hh24:mi:ss'),
	40
);

INSERT INTO Aanbieding(Product_id, datumBegin, datumEinde, prijs)
values(
	3,
	TO_DATE('05/04/2016 09:30:22', 'dd/mm/yyyy hh24:mi:ss'),
	TO_DATE('09/04/2016 09:30:22', 'dd/mm/yyyy hh24:mi:ss'),
	10
);

INSERT INTO Aanbieding(Product_id, datumBegin, datumEinde, prijs)
values(
	4,
	TO_DATE('07/04/2016 14:50:10', 'dd/mm/yyyy hh24:mi:ss'),
	TO_DATE('08/04/2016 14:50:10', 'dd/mm/yyyy hh24:mi:ss'),
	1000
);

-- Insert test Nieuwsbrief --

INSERT INTO Nieuwsbrief(naam, beschrijving)
values(
	'DagDeals',
	'Mis geen DagDeal meer! Ontvang dagelijks de aanbieding van de dag in uw elektronische brievenbus.'
);

INSERT INTO Nieuwsbrief(naam, beschrijving)
values(
	'Informatie rondom je aankoop',
	'Reviewverzoeken, artikelsuggesties en informatie over onze artikelen en –diensten op basis van je eerdere aankopen.'
);

INSERT INTO Nieuwsbrief(naam, beschrijving)
values(
	'Inspiratie & acties',
	'Persoonlijke aanbevelingen en informatie op basis van je aankopen, -bezoekgeschiedenis en algemene (win)acties.'
);

INSERT INTO Nieuwsbrief(naam, beschrijving)
values(
	'Zakelijke nieuwsbrief',
	'Ontvang maandelijks de beste zakelijke aanbiedingen en informatie over al je zakelijke bestellingen.'
);

-- Insert test GebruikerNieuwsbrief --

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'1',
	'1',
	TO_DATE('06/04/2016 10:30:25', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'1',
	'2',
	TO_DATE('06/03/2016 10:30:22', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'1',
	'3',
	TO_DATE('06/02/2016 23:30:22', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'1',
	'4',
	TO_DATE('10/04/2016 20:35:22', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'2',
	'1',
	TO_DATE('15/04/2015 08:30:22', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'2',
	'2',
	TO_DATE('20/04/2013 09:30:22', 'dd/mm/yyyy hh24:mi:ss')
);

INSERT INTO GebruikerNieuwsbrief(Gebruiker_id, Nieuwsbrief_id, datumStart)
values(
	'4',
	'1',
	TO_DATE('26/09/2014 09:30:22', 'dd/mm/yyyy hh24:mi:ss')
);

-- Insert test Reviews --

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'1',
	'1',
	TO_DATE('26/09/2014 09:35:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Erg goeie game!',
	'5'
);

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'1',
	'2',
	TO_DATE('26/09/2014 19:30:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Minder leuke game.',
	'2'
);

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'3',
	'3',
	TO_DATE('26/09/2014 22:30:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Geweldig album.',
	'5'
);

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'3',
	'1',
	TO_DATE('28/10/2015 15:30:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Gaat ermee door.',
	'3'
);

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'5',
	'1',
	TO_DATE('15/06/2015 09:35:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Fifa is geweldig!',
	'5'
);

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'6',
	'1',
	TO_DATE('20/09/2015 09:35:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Leuk :)',
	'4'
);

INSERT INTO Review(Gebruiker_id, Product_id, datum, review, rating)
values(
	'7',
	'1',
	TO_DATE('26/09/2015 09:35:22', 'dd/mm/yyyy hh24:mi:ss'),
	'Beste spel ooit.',
	'5'
);

-- Insert test Adres --

INSERT INTO Adres(postcode, huisnummer, naam)
values(
	'5611AB',
	'1b',
	'Piet Paulusma'
);

INSERT INTO Adres(postcode, huisnummer, naam)
values(
	'5615AB',
	'22',
	'Bob Smit'
);

INSERT INTO Adres(postcode, huisnummer, naam, bedrijfsnaam)
values(
	'3730HR',
	'2',
	'Tim de Groot',
	'Albert Heijn'
);

INSERT INTO Adres(postcode, huisnummer, naam)
values(
	'5787HT',
	'5012',
	'Pim Pam'
);

INSERT INTO Adres(postcode, huisnummer, naam)
values(
	'6742AS',
	'798',
	'Paula Bakker'
);

INSERT INTO Adres(postcode, huisnummer, naam)
values(
	'5648KH',
	'68',
	'Tom de Vries'
);

INSERT INTO Adres(postcode, huisnummer, naam, bedrijfsnaam)
values(
	'6489LJ',
	'790',
	'Bert Visser',
	'Viswinkel'
);

-- Insert test GebruikerAdres --

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	9,
	'3730HR',
	'2'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	2,
	'5615AB',
	'22'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	1,
	'5611AB',
	'1b'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	2,
	'3730HR',
	'2'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	3,
	'3730HR',
	'2'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	4,
	'5787HT',
	'5012'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	5,
	'6742AS',
	'798'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	6,
	'5648KH',
	'68'
);

INSERT INTO GebruikerAdres(Gebruiker_id, Adres_postcode, Adres_huisnummer)
values(
	7,
	'6489LJ',
	'790'
);

-- Insert test WinkelwagenProduct --

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	1,
	2
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	2,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	3,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	4,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	5,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	6,
	8
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	7,
	4
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	1,
	8,
	3
);	
	
INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	2,
	5,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	2,
	6,
	8
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	2,
	7,
	4
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	2,
	8,
	3
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	3,
	1,
	2
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	3,
	2,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	3,
	3,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	3,
	4,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	4,
	1,
	2
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	4,
	2,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	4,
	3,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	4,
	4,
	1
);
	
INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	6,
	7,
	2
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	6,
	8,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	6,
	3,
	1
);

INSERT INTO WinkelwagenProduct(Gebruiker_id, Product_id, aantal)
values(
	6,
	4,
	1
);

-- Insert test Foto --

INSERT INTO Foto(Product_id, url)
values(
	'1',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/2/2/4/6/9200000045476422.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'2',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/4/2/7/7/9200000045947724.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'3',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/1/9/1/2/9200000049602191.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'4',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/8/5/2/8/9200000048968258.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'4',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/8/5/2/8/9200000048968258_2.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'5',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/4/5/6/2/9200000047822654.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'6',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/7/8/0/7/9200000050037087.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'7',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/9/1/8/9/9200000051639819.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'8',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/5/0/2/0/9200000040930205.jpg'
);

INSERT INTO Foto(Product_id, url)
values(
	'9',
	'https://s.s-bol.com/imgbase0/imagebase3/large/FC/6/5/2/2/9200000053832256.jpg'
);

-- The records below won't have a picture, and as entity Foto has a default value of url, it will insert the default url for us.
INSERT INTO Foto(Product_id)
values(
	'10'
);

INSERT INTO Foto(Product_id)
values(
	'11'
);

INSERT INTO Foto(Product_id)
values(
	'12'
);
commit;