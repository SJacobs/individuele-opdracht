﻿namespace BolUnitTest
{
    using System;
    using Bol;
    using Bol.Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class UnitTest1
    {
        private Category category = new Category(1, "testcategory");
        private Product product = new Product(1, 1, "testproduct", 5, "testbrand", "testtype", true, "prop", "desc");

        [TestMethod]
        public void Account()
        {
            Account account = new Account(1, "testemail@gmail.com", "Test123");
            Account account2 = new Account(1, "testemail@gmail.com", "Test123");
            account.ToString();
            Assert.AreEqual(account.Id, 1);
            Assert.AreEqual(account.Email, "testemail@gmail.com");
            Assert.AreEqual(account.Password, "Test123");
            Assert.AreEqual(account, account2);
        }

        [TestMethod]
        public void Category()
        {
            category.ParentCategoryId = 1;
            Assert.AreEqual(category.Id, 1);
            Assert.AreEqual(category.Name, "testcategory");
            Assert.AreEqual(category.ParentCategoryId, 1);
            Assert.AreEqual(category.ToString(), category.Id + ". " + category.Name);
        }

        [TestMethod]
        public void Product()
        {
            Product product = new Product(1, 1, "testproduct", 5, "testbrand", "testtype", true, "prop", "desc");
            Assert.AreEqual(product.Id, 1);
            Assert.AreEqual(product.CategoryId, 1);
            Assert.AreEqual(product.Description, "desc");
            Assert.AreEqual(product.Name, "testproduct");
            Assert.AreEqual(product.Price, 5);
            Assert.AreEqual(product.Properties, "prop");
            Assert.AreEqual(product.Type, "testtype");
            Assert.AreEqual(product.Used, true);
        }

        [TestMethod]
        public void ProductWithAmount()
        {
            ProductWithAmount productWithAmount = new ProductWithAmount(product, 15);
            Assert.AreEqual(productWithAmount.Product, product);
        }

        [TestMethod]
        public void ProductWithCategory()
        {
            ProductWithCategory productWithCategory = new ProductWithCategory(product, category);
            Assert.AreEqual(productWithCategory.Product, product);
        }
    }
}
