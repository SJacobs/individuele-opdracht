﻿using System.Web.Mvc;
using Bol.Models;
using Bol.Repository;

namespace Bol.Controllers
{
    public class WishlistController : Controller
    {
        private WishlistRepository wishlistRepo = new WishlistRepository();
        private AccountRepository accountRepo = new AccountRepository();
        private ProductRepository productRepo = new ProductRepository();

        /// <summary>
        /// Wish list view.
        /// </summary>
        /// <returns>View with all wish lists.</returns>
        public ActionResult Index()
        {
            if (!loggedIn())
            {
                return RedirectToAction("Login", "Account");
            }

            return View(wishlistRepo.GetProducts(loggedInAccount()));
        }

        /// <summary>
        /// Add product to wish list.
        /// </summary>
        /// <param name="id">The id of the product to add.</param>
        /// <returns>Add product view.</returns>
        public ActionResult AddProduct(int? id)
        {
            if (!loggedIn())
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Product product = productRepo.GetById((int)id);
            wishlistRepo.AddProduct(loggedInAccount(), product);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Delete product from wish list.
        /// </summary>
        /// <param name="id">The id of the product to delete.</param>
        /// <returns>Delete product view.</returns>
        public ActionResult DeleteProduct(int? id)
        {
            if (!loggedIn())
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }
            
            Product product = productRepo.GetById((int)id);
            wishlistRepo.DeleteProduct(loggedInAccount(), product);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Gets the logged in account.
        /// </summary>
        /// <returns>Logged in account.</returns>
        private Account loggedInAccount()
        {
            Account account = null;
            if (loggedIn())
            {
                account = accountRepo.getAccount(Session["email"].ToString());
            }

            return account;
        }

        private bool loggedIn()
        {
            return Session["email"] != null;
        }
    }
}