﻿namespace Bol.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using Bol.Models;
    using Bol.Repository;

    /// <summary>
    /// The account controller.
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// Account repository, anything related to accounts will be done here.
        /// </summary>
        private AccountRepository accountRepo = new AccountRepository();

        /// <summary>
        /// Show all accounts.
        /// </summary>
        /// <returns>Returns a view with all accounts.</returns>
        public ActionResult Index()
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return View(accountRepo.GetAll());
        }

        /// <summary>
        /// This shows your profile in a view.
        /// </summary>
        /// <returns>View with account.</returns>
        public ActionResult Profile()
        {
            var email = Session["Email"];
            Account account = null;
            if (email == null)
            {
                return RedirectToAction("Login", "Account");
            }

            account = accountRepo.getAccount(email.ToString());
            return View(account);
        }

        /// <summary>
        /// This gets called when you edit your profile.
        /// </summary>
        /// <param name="account">Account to view.</param>
        /// <returns>View with account.</returns>
        [HttpPost]
        public ActionResult Profile(Account account)
        {
            if (!ModelState.IsValid)
            {
                Debug.WriteLine(account.DateOfBirth);
                ViewData["error"] = "Invalid field";
                return View(account);
            }

            if (!isProfileValid(account))
            {
                return View(account);
            }

            if (emailTaken(Session["email"].ToString(), account.Email))
            {
                return View(account);
            }

            // account doesn't receive id and admin from view so we'll set that manually
            Account loggedIn = loggedInAccount();
            account.Id = loggedIn.Id;
            account.isAdmin = loggedIn.isAdmin;

            // Get date of birth parameter from input
            DateTime dateOfBirth;
            DateTime.TryParse(Request.Params.Get("dob"), out dateOfBirth);
            account.DateOfBirth = dateOfBirth;

            if (!accountRepo.UpdateAccount(account))
            {
                ViewData["Error"] = "Invalid input!";
                return View(account);
            }

            Session["email"] = account.Email;
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Shows add account view.
        /// </summary>
        /// <returns>Returns the add view.</returns>
        [HttpGet]
        public ActionResult Add()
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        /// <summary>
        /// Adds account, admins only.
        /// </summary>
        /// <param name="account">Account to add.</param>
        /// <returns>Returns home index view.</returns>
        [HttpPost]
        public ActionResult Add(Account account)
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                ViewData["error"] = "Invalid field";
                return View(account);
            }

            // Get date of birth parameter from input
            DateTime dateOfBirth;
            DateTime.TryParse(Request.Params.Get("dob"), out dateOfBirth);
            account.DateOfBirth = dateOfBirth;

            if (!isRegisterValid(account))
            {
                return View(account);
            }

            if (!accountRepo.AddAccount(account))
            {
                ViewData["Error"] = "Invalid input!";
                return View(account);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Shows the form to register an account.
        /// </summary>
        /// <returns>Register view.</returns>
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// An account has been registered.
        /// </summary>
        /// <param name="account">Account to register.</param>
        /// <returns>View with account if exception. Else redirect to Index view.</returns>
        [HttpPost]
        public ActionResult Register(Account account)
        {
            if (!ModelState.IsValid)
            {
                Debug.WriteLine(account.DateOfBirth);
                ViewData["error"] = "Invalid field";
                return View(account);
            }

            if (!isRegisterValid(account))
            {
                return View(account);
            }

            if (!accountRepo.AddAccount(account))
            {
                ViewData["Error"] = "Invalid input!";
                return View(account);
            }

            FormsAuthentication.SetAuthCookie(account.Email, true, account.Password);
            Session["Email"] = account.Email;
            Session["Password"] = account.Password;
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Shows the login form.
        /// </summary>
        /// <returns>Returns login view.</returns>
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Happens when a user is actually logging in.
        /// </summary>
        /// <param name="account">The account to login.</param>
        /// <returns>Returns home index view.</returns>
        [HttpPost]
        public ActionResult Login(Account account)
        {
            if (!ModelState.IsValid)
            {
                Debug.WriteLine(account.DateOfBirth);
                ViewData["error"] = "Invalid field";
                return View(account);
            }

            if (!accountRepo.IsAccountValid(account))
            {
                ViewData["Error"] = "Email or password is invalid.";
                return View(account);
            }

            FormsAuthentication.SetAuthCookie(account.Email, true, account.Password);
            Session["Email"] = account.Email;
            Session["Password"] = account.Password;
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Log out account.
        /// </summary>
        /// <returns>Returns home index view.</returns>
        public ActionResult LogOut()
        {
            Session["Email"] = null;
            Session["Password"] = null;
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Edit an account, admins only.
        /// </summary>
        /// <param name="id">The id of the account.</param>
        /// <returns>Returns a view with account.</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            Account account = accountRepo.getAccount((int)id);
            return View(account);
        }

        /// <summary>
        /// Account has been edited.
        /// </summary>
        /// <param name="account">The account to edit.</param>
        /// <returns>Returns home index view.</returns>
        [HttpPost]
        public ActionResult Edit(Account account)
        {
            if (!ModelState.IsValid)
            {
                Debug.WriteLine(account.DateOfBirth);
                ViewData["error"] = "Invalid field";
                return View(account);
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            // Get date of birth parameter from input
            DateTime dateOfBirth;
            DateTime.TryParse(Request.Params.Get("dob"), out dateOfBirth);
            account.DateOfBirth = dateOfBirth;

            if (!accountRepo.UpdateAccount(account))
            {
                ViewData["error"] = "Invalid field!";
                return View(account);
            }

            if (accountRepo.getAccount(Session["email"].ToString()).Id == account.Id)
            {
                Session["email"] = account.Email;
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Gets the currently logged in account.
        /// </summary>
        /// <returns>Returns the logged in account.</returns>
        private Account loggedInAccount()
        {
            Account account = null;
            if (Session["email"] != null)
            {
                account = accountRepo.getAccount(Session["email"].ToString());
            }

            return account;
        }

        /// <summary>
        /// Checks if an admin is logged in currently.
        /// </summary>
        /// <returns>Returns whether the account is logged in.</returns>
        private bool loggedInAsAdmin()
        {
            if (Session["email"] == null || !accountRepo.isAdmin(Session["email"].ToString()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks if the registered account is valid.
        /// </summary>
        /// <param name="account">The account to register.</param>
        /// <returns>Returns whether the account is valid.</returns>
        private bool isRegisterValid(Account account)
        {
            bool valid = true;
            if (account.Email == null || account.Password == null)
            {
                ViewData["Error"] = "Please fill in all fields.";
                return false;
            }
            else if (accountRepo.AccountExists(account))
            {
                ViewData["Error"] = "Email has been registered already.";
                return false;
            }
            else if (account.Password.Length < 6)
            {
                ViewData["Error"] = "Password too short, length has to be atleast 6.";
                valid = false;
            }
            else if (account.Password.Length > 20)
            {
                ViewData["Error"] = "Password too long, length has to be less than 20.";
                valid = false;
            }
            else if (account.Email.Length >= 100)
            {
                ViewData["Error"] = "Email is too long, length has to be less than 100";
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// Checks if the edited profile is valid.
        /// </summary>
        /// <param name="account">The account to check.</param>
        /// <returns>Returns whether the account is valid.</returns>
        private bool isProfileValid(Account account)
        {
            bool valid = true;
            if (account.Email == null || account.Password == null)
            {
                ViewData["Error"] = "Please fill in all fields.";
                return false;
            }
            else if (account.Password.Length < 6)
            {
                ViewData["Error"] = "Password too short, length has to be atleast 6.";
                valid = false;
            }
            else if (account.Password.Length > 20)
            {
                ViewData["Error"] = "Password too long, length has to be less than 20.";
                valid = false;
            }
            else if (account.Email.Length >= 100)
            {
                ViewData["Error"] = "Email is too long, length has to be less than 100";
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// Checks if email is taken.
        /// </summary>
        /// <param name="myEmail">Old email string.</param>
        /// <param name="newEmail">New email string.</param>
        /// <returns>Return whether the email is taken.</returns>
        private bool emailTaken(string myEmail, string newEmail)
        {
            if (myEmail != newEmail && accountRepo.EmailTaken(newEmail))
            {
                ViewData["Error"] = "Invalid input!";
                return true;
            }

            return false;
        }
    }
}