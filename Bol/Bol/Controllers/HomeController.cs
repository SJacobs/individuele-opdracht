﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bol.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Home view.
        /// </summary>
        /// <returns>The home view.</returns>
        public ActionResult Index()
        {
            // return View();
            return RedirectToAction("Index", "Category");
        }

        /// <summary>
        /// About view.
        /// </summary>
        /// <returns>The about view.</returns>
        public ActionResult About()
        {
            return View();
        }
    }
}