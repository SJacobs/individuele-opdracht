﻿using System.Web.Mvc;
using Bol.Models;
using Bol.Repository;
using Bol.Repository.ShoppingCart;

namespace Bol.Controllers
{
    /// <summary>
    /// The product controller.
    /// </summary>
    public class ProductController : Controller
    {
        private ProductRepository productRepo = new ProductRepository();
        private AccountRepository accountRepo = new AccountRepository();
        private ShoppingCartRepository cartRepo = new ShoppingCartRepository();

        /// <summary>
        /// The product index view.
        /// </summary>
        /// <returns>View with all products.</returns>
        public ActionResult Index()
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return View(productRepo.GetAll());
        }

        /// <summary>
        /// View product view.
        /// </summary>
        /// <param name="id">The id of the product to view.</param>
        /// <returns>View the product view.</returns>
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Product product = productRepo.GetById((int)id);
            return View(product);
        }

        /// <summary>
        /// The account currently logged in.
        /// </summary>
        /// <returns>Account currently logged in.</returns>
        private Account getLoggedIn()
        {
            Account account = null;
            if (Session["email"] != null)
            {
                account = accountRepo.getAccount(Session["email"].ToString());
            }

            return account;
        }

        /// <summary>
        /// Checks whether the account currently logged in is an admin.
        /// </summary>
        /// <returns>Whether an admin is logged in.</returns>
        private bool loggedInAsAdmin()
        {
            if (Session["email"] == null || !accountRepo.isAdmin(Session["email"].ToString()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks whether someone is currently logged in.
        /// </summary>
        /// <returns>Whether someone is logged in.</returns>
        private bool loggedIn()
        {
            if (Session["email"] == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the edit product view.
        /// </summary>
        /// <param name="id">The id of the product to edit.</param>
        /// <returns>Returns edit product view.</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            Product product = productRepo.GetById((int)id);
            return View(product);
        }

        /// <summary>
        /// Edit product.
        /// </summary>
        /// <param name="product">The product to edit.</param>
        /// <returns>Return edit product view.</returns>
        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (!ModelState.IsValid)
            {
                return View(product);
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            if (!productRepo.UpdateProduct(product))
            {
                ViewData["error"] = "Invalid field!";
                return View(product);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Add product view.
        /// </summary>
        /// <returns>The add product view.</returns>
        [HttpGet]
        public ActionResult Add()
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Add(Product product)
        {
            if (!ModelState.IsValid)
            {
                return View(product);
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            if (!productRepo.AddProduct(product))
            {
                ViewData["error"] = "Invalid field!";
                return View(product);
            }

            return RedirectToAction("Index");
        }
    }
}