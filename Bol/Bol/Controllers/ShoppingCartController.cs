﻿using System.Web.Mvc;
using Bol.Models;
using Bol.Repository;
using Bol.Repository.ShoppingCart;

namespace Bol.Controllers
{
    public class ShoppingCartController : Controller
    {
        private ShoppingCartRepository shoppingCartRepository = new ShoppingCartRepository();
        private AccountRepository accountRepository = new AccountRepository();
        private ProductRepository productRepository = new ProductRepository();

        /// <summary>
        /// The index view of shopping cart.
        /// </summary>
        /// <returns>The shopping cart is view.</returns>
        public ActionResult Index()
        {
            if (!loggedIn())
            {
                return RedirectToAction("Login", "Account");
            }

            Account account = accountRepository.getAccount(Session["email"].ToString());
            return View(shoppingCartRepository.GetAll(account));
        }

        /// <summary>
        /// Add product view.
        /// </summary>
        /// <param name="id">Add product.</param>
        /// <returns>Returns the add product view.</returns>
        public ActionResult AddProduct(int? id)
        {
            if (!loggedIn())
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Product product = productRepository.GetById((int)id);
            shoppingCartRepository.AddProduct(loggedInAccount().Id, product);
            return RedirectToAction("Index", "ShoppingCart");
        }

        /// <summary>
        /// Delete product from shopping cart.
        /// </summary>
        /// <param name="id">The id of the product to delete.</param>
        /// <returns>The delete product view.</returns>
        public ActionResult DeleteProduct(int? id)
        {
            if (!loggedIn())
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            Product product = productRepository.GetById((int)id);
            shoppingCartRepository.DeleteProduct(loggedInAccount().Id, product);
            return RedirectToAction("Index", "ShoppingCart");
        }

        /// <summary>
        /// The logged in account.
        /// </summary>
        /// <returns>Logged in account.</returns>
        private Account loggedInAccount()
        {
            Account account = null;
            if (loggedIn())
            {
                account = accountRepository.getAccount(Session["email"].ToString());
            }

            return account;
        }

        private bool loggedIn()
        {
            return Session["email"] != null;
        }
    }
}