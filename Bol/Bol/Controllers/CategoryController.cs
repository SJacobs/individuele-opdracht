﻿namespace Bol.Controllers
{
    using System.Web.Mvc;
    using Bol.Repository;
    using Models;

    public class CategoryController : Controller
    {
        private CategoryRepository categoryRepo = new CategoryRepository();
        private AccountRepository accountRepo = new AccountRepository();

        /// <summary>
        /// The view with all categories.
        /// </summary>
        /// <returns>Returns the view with categories.</returns>
        public ActionResult Index()
        {
            return View(categoryRepo.GetAll());
        }

        /// <summary>
        /// The view of a single category.
        /// </summary>
        /// <param name="id">The id of the category.</param>
        /// <returns>The category view.</returns>
        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Category category = categoryRepo.GetCategory((int)id);
            return View(category);
        }

        /// <summary>
        /// Checks whether an admin is currently logged in.
        /// </summary>
        /// <returns>Returns whether an admin is logged in.</returns>
        private bool loggedInAsAdmin()
        {
            if (Session["email"] == null || !accountRepo.isAdmin(Session["email"].ToString()))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Edit a category.
        /// </summary>
        /// <param name="id">The id of the category to edit.</param>
        /// <returns>Returns an edit view.</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            Category category = categoryRepo.GetCategory((int)id);
            return View(category);
        }

        /// <summary>
        /// Edit category view.
        /// </summary>
        /// <param name="category">Category to edit.</param>
        /// <returns>The edit category view.</returns>
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }

            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            if (!categoryRepo.UpdateCategory(category))
            {
                ViewData["error"] = "Invalid field!";
                return View(category);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Add category view.
        /// </summary>
        /// <returns>Add the category view.</returns>
        [HttpGet]
        public ActionResult Add()
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        /// <summary>
        /// Add category view.
        /// </summary>
        /// <param name="category">The category to add.</param>
        /// <returns>View with category.</returns>
        [HttpPost]
        public ActionResult Add(Category category)
        {
            if (!loggedInAsAdmin())
            {
                return RedirectToAction("Index", "Home");
            }

            if (!categoryRepo.AddCategory(category))
            {
                ViewData["error"] = "Invalid field!";
                return View(category);
            }

            return RedirectToAction("Index");
        }
    }
}