﻿namespace Bol.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class AdminController : Controller
    {
        /// <summary>
        /// Shows the admin panel.
        /// </summary>
        /// <returns>Returns the admin view.</returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}