﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bol.Models
{
    public class ProductWithCategory
    {
        /// <summary>
        /// Gets or sets product.
        /// </summary>
        /// <value>The product.</value>
        public Product Product { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>The category of the product.</value>
        public Category Category { get; set; }

        public ProductWithCategory()
        {
        }

        public ProductWithCategory(Product product, Category category)
        {
            Product = product;
            Category = category;
        }
    }
}