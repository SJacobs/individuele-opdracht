﻿namespace Bol.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class Account
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id of the account.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the account is admin.
        /// </summary>
        /// <value>Account is admin.</value>
        [Display(Name = "Admin")]
        public bool isAdmin { get; set; }

        /// <summary>
        /// Gets or sets the name of account.
        /// </summary>
        /// <value>The name of the account.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email of the account.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password of the account.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>The date of birth of the account.</value>
        [DisplayName("Date of birth")]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>The phone number of the account.</value>
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Account"/> class.
        /// </summary>
        public Account()
        {
        }

        public Account(int id, string email, string password)
        {
            Id = id;
            Email = email;
            Password = password;
        }

        /// <summary>
        /// Describes the account.
        /// </summary>
        /// <returns>A description of the account.</returns>
        public override string ToString()
        {
            return Id + ". Email: " + Email + ", Password: " + Password;
        }
    }
}