﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bol.Models
{
    public class Product
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id of the product.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the category id.
        /// </summary>
        /// <value>The category id of the product.</value>
        public int CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name of the product.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price of the product.</value>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the brand.
        /// </summary>
        /// <value>The brand of the product.</value>
        public string Brand { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type of the product.</value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the product is used.
        /// </summary>
        /// <value>The product is used.</value>
        public bool Used { get; set; }

        /// <summary>
        /// Gets or sets the properties.
        /// </summary>
        /// <value>The properties of the product.</value>
        public string Properties { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description of the product.</value>
        public string Description { get; set; }

        public Product()
        {
        }

        public Product(int id, int categoryId, string name, decimal price, string brand, string type, bool used, string properties, string description)
        {
            Id = id;
            CategoryId = categoryId;
            Name = name;
            Price = price;
            Brand = brand;
            Type = type;
            Used = used;
            Properties = properties;
            Description = description;
        }

        /// <summary>
        /// The description of the product.
        /// </summary>
        /// <returns>The description of product.</returns>
        public override string ToString()
        {
            return Id + ". " + Name + " " + Price + " eur";
        }
    }
}