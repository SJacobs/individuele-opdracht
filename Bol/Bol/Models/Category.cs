﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Bol.Models
{
    public class Category
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        /// <value>The id of the category.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        /// <value>The name of the category.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the list of products.
        /// </summary>
        /// <value>The products of the category.</value>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Gets or sets the parent category id.
        /// </summary>
        /// <value>The parent category id of the category.</value>
        [DisplayName("Parent category id")]
        public int ParentCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the child categories.
        /// </summary>
        /// <value>The child categories of the category.</value>
        public List<Category> ChildCategories { get; set; }

        public Category()
        {
        }

        public Category(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Category(int id, string name, int parentCategoryId, List<Category> childCategories)
        {
            Id = id;
            Name = name;
            ParentCategoryId = parentCategoryId;
            ChildCategories = childCategories;
        }

        /// <summary>
        /// Description of the category.
        /// </summary>
        /// <returns>The description of the category.</returns>
        public override string ToString()
        {
            return Id + ". " + Name;
        }
    }
}