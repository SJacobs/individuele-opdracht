﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bol.Models
{
    public class ProductWithAmount
    {
        /// <summary>
        /// Gets or sets the product.
        /// </summary>
        /// <value>The product.</value>
        public Product Product { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount of products.</value>
        public int Amount { get; set; }

        public ProductWithAmount(Product product, int amount)
        {
            Product = product;
            Amount = amount;
        }

        public override string ToString()
        {
            return Product + ", amount:" + Amount;
        }
    }
}