﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bol.Models;

namespace Bol.Repository
{
    public interface IProductContext
    {
        List<Product> GetAll();

        Product GetByName(string name);

        Product GetById(int id);

        bool AddProduct(Product product);

        bool UpdateProduct(Product product);

        bool DeleteProduct(Product product);
    }
}