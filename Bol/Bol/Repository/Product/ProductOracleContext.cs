﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Bol.Models;
using Oracle.ManagedDataAccess.Client;

namespace Bol.Repository
{
    public class ProductOracleContext : IProductContext
    {
        private OracleDatabaseHandler databaseHandler = new OracleDatabaseHandler();

        public bool AddProduct(Product product)
        {
            string query = string.Format("INSERT INTO Product(Categorie_id, naam, prijs, merk, type, gebruikt, eigenschappen, beschrijving)values({0}, '{1}', {2}, '{3}', '{4}', {5}, '{6}', '{7}')", product.CategoryId, product.Name, product.Price, product.Brand, product.Type, (product.Used ? 1 : 0), product.Properties, product.Description);
            return databaseHandler.ExecuteQuery(query);
        }

        public bool UpdateProduct(Product product)
        {
            string query =
                "UPDATE product SET " +
                "Categorie_id   = :0," +
                "naam           = :1," +
                "prijs          = :2," +
                "merk           = :3," +
                "type           = :4," +
                "gebruikt       = :5," +
                "eigenschappen  = :6," +
                "beschrijving   = :7 " +
                "where id       = :8";

            OracleCommand command = new OracleCommand();
            command.CommandText = query;
            
            command.Parameters.Add(new OracleParameter("0", OracleDbType.Int32, product.CategoryId, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("1", OracleDbType.Varchar2, product.Name, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("2", OracleDbType.Decimal, product.Price, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("3", OracleDbType.Varchar2, product.Brand, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("4", OracleDbType.Varchar2, product.Type, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("5", OracleDbType.Int32, (product.Used ? 1 : 0), ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("6", OracleDbType.Varchar2, product.Properties, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("7", OracleDbType.Varchar2, product.Description, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("8", OracleDbType.Int32, product.Id, ParameterDirection.Input));

            return databaseHandler.ExecuteQuery(command);
        }

        public List<Product> GetAll()
        {
            List<Product> products = new List<Product>();

            DataTable dt = databaseHandler.ReadQuery("select * from product");
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"].ToString());
                int categoryId = Convert.ToInt32(row["categorie_id"].ToString());
                string name = row["naam"].ToString();
                decimal price = Convert.ToDecimal(row["prijs"].ToString());
                string brand = row["merk"].ToString();
                string type = row["type"].ToString();
                bool used = (Convert.ToInt32(row["gebruikt"].ToString()) == 1 ? true : false);
                string properties = row["eigenschappen"].ToString();
                string description = row["beschrijving"].ToString();

                Product product = new Product(id, categoryId, name, price, brand, type, used, properties, description);
                products.Add(product);
            }

            return products;
        }

        public Product GetById(int productId)
        {
            Product product = null;
            DataTable dt = databaseHandler.ReadQuery(string.Format("select * from product where id = {0}", productId));
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"].ToString());
                int categoryId = Convert.ToInt32(row["categorie_id"].ToString());
                string name = row["naam"].ToString();
                decimal price = Convert.ToDecimal(row["prijs"].ToString());
                string brand = row["merk"].ToString();
                string type = row["type"].ToString();
                bool used = (Convert.ToInt32(row["gebruikt"].ToString()) == 1 ? true : false);
                string properties = row["eigenschappen"].ToString();
                string description = row["beschrijving"].ToString();

                product = new Product(id, categoryId, name, price, brand, type, used, properties, description);
            }

            return product;
        }

        public Product GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool DeleteProduct(Product product)
        {
            string query = string.Format("DELETE FROM product WHERE id = {0}", product.Id);
            return databaseHandler.ExecuteQuery(query);
        }
    }
}