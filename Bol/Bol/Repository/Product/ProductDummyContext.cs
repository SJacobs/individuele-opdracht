﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bol.Models;

namespace Bol.Repository
{
    public class ProductDummyContext : IProductContext
    {
        private static List<Product> products = new List<Product>()
            {
            // dummy products
            };

        public bool AddProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public bool UpdateProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public List<Product> GetAll()
        {
            return products;
        }

        public Product GetById(int id)
        {
            foreach (Product product in GetAll())
            {
                if (product.Id == id)
                {
                    return product;
                }
            }

            return null;
        }

        public Product GetByName(string name)
        {
            throw new NotImplementedException();
        }

        private int getLastId()
        {
            return products[products.Count - 1].Id;
        }

        public bool DeleteProduct(Product product)
        {
            throw new NotImplementedException();
        }
    }
}