﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public class ProductRepository
    {
        private IProductContext context;

        public ProductRepository()
        {
            // context = new ProductDummyContext();
            context = new ProductOracleContext();
        }

        public ProductRepository(IProductContext context)
        {
            this.context = context;
        }

        public List<Product> GetAll()
        {
            return context.GetAll();
        }

        public Product GetByName(string name)
        {
            return context.GetByName(name);
        }

        public Product GetById(int id)
        {
            return context.GetById(id);
        }

        public bool UpdateProduct(Product product)
        {
            return context.UpdateProduct(product);
        }

        public bool AddProduct(Product product)
        {
            return context.AddProduct(product);
        }

        public bool DeleteProduct(Product product)
        {
            return context.DeleteProduct(product);
        }
    }
}