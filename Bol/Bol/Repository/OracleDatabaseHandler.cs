﻿using System;
using System.Data;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;

namespace Bol.Repository
{
    public class OracleDatabaseHandler
    {
        private OracleConnection connection;

        public OracleDatabaseHandler()
        {
            connection = new OracleConnection();
            connection.ConnectionString = Settings.ConnectionString;
        }

        public bool ExecuteQuery(string query)
        {
            try
            {
                openConnection();
                OracleCommand command = new OracleCommand(query, connection);
                command.ExecuteNonQuery();
                closeConnection();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("EXECUTE QUERY ERROR: " + e.Message);
                return false;
            }
        }

        public bool ExecuteQuery(OracleCommand command)
        {
            try
            {
                openConnection();
                command.Connection = connection;
                command.ExecuteNonQuery();
                closeConnection();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("EXECUTE QUERY ERROR: " + e.Message);
                return false;
            }
        }

        public DataTable ReadQuery(string query)
        {
            DataTable dt = new DataTable();
            this.openConnection();

            OracleCommand command = new OracleCommand(query, connection);

            using (OracleDataAdapter dataAdapter = new OracleDataAdapter(command))
            {
                dataAdapter.Fill(dt);
            }

            this.closeConnection();

            return dt;
        }

        private void openConnection()
        {
            if (this.connection.State != ConnectionState.Open)
            {
                this.connection.Open();
            }
        }

        private void closeConnection()
        {
            if (this.connection.State != ConnectionState.Closed)
            {
                this.connection.Close();
            }
        }
    }
}