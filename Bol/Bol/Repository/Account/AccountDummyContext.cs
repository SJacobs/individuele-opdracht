﻿using System;
using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public class AccountDummyContext : IAccountContext
    {
        private static List<Account> accounts = new List<Account>
        {
            new Account(1, "simin", "test"),
            new Account(2, "piet", "test")
         };

        /// <summary>
        /// Checks if email is taken.
        /// </summary>
        /// <param name="account">The email to check.</param>
        /// <returns>Returns whether the email is taken.</returns>
        public bool emailTaken(Account account)
        {
            throw new NotImplementedException();
        }

        public bool AddAccount(Account account)
        {
            if (account.Id == 0)
            {
                account.Id = GetLastId() + 1;
            }

            accounts.Add(account);
            return true;
        }

        /// <summary>
        /// Updates an account.
        /// </summary>
        /// <param name="account">The account to update.</param>
        /// <returns>Returns whether the account is updated.</returns>
        public bool UpdateAccount(Account account)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks whether the email is taken.
        /// </summary>
        /// <param name="email">The email to check.</param>
        /// <returns>Returns whether the email is taken.</returns>
        public bool EmailTaken(string email)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all accounts.
        /// </summary>
        /// <returns>Returns all accounts.</returns>
        public List<Account> GetAll()
        {
            return accounts;
        }

        /// <summary>
        /// Gets the id of the last account.
        /// </summary>
        /// <returns>Returns the id.</returns>
        public int GetLastId()
        {
            return accounts[accounts.Count - 1].Id;
        }

        /// <summary>
        /// Checks if the account is existent.
        /// </summary>
        /// <param name="account">The account to check.</param>
        /// <returns>Whether account exists.</returns>
        public bool IsAccountValid(Account account)
        {
            return accounts.Contains(account);
        }

        /// <summary>
        /// Checks if account exists.
        /// </summary>
        /// <param name="account">The account to check.</param>
        /// <returns>Returns if the account exists.</returns>
        public bool AccountExists(Account account)
        {
            throw new NotImplementedException();
        }
    }
}