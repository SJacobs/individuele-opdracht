﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public interface IAccountContext
    {
        /// <summary>
        /// Gets all accounts.
        /// </summary>
        /// <returns>The accounts.</returns>
        List<Account> GetAll();

        /// <summary>
        /// Add an account.
        /// </summary>
        /// <param name="account">Account to add.</param>
        /// <returns>Whether the account is added.</returns>
        bool AddAccount(Account account);

        /// <summary>
        /// Update account.
        /// </summary>
        /// <param name="account">Account to update.</param>
        /// <returns>Whether the account is updated.</returns>
        bool UpdateAccount(Account account);

        /// <summary>
        /// Checks whether the account is valid.
        /// </summary>
        /// <param name="account">Account to check.</param>
        /// <returns>Whether the account is valid.</returns>
        bool IsAccountValid(Account account);

        /// <summary>
        /// Checks whether the account exists.
        /// </summary>
        /// <param name="account">Account to check.</param>
        /// <returns>Whether the account exists.</returns>
        bool AccountExists(Account account);

        /// <summary>
        /// Checks whether the email is taken.
        /// </summary>
        /// <param name="email">Email to check.</param>
        /// <returns>Whether the email is taken.</returns>
        bool EmailTaken(string email);
    }
}