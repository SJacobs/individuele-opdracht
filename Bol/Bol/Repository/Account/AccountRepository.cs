﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public class AccountRepository
    {
        private IAccountContext context;

        public AccountRepository()
        {
            context = new AccountOracleContext();
        }

        public AccountRepository(IAccountContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets all accounts.
        /// </summary>
        /// <returns>All accounts.</returns>
        public List<Account> GetAll()
        {
            return context.GetAll();
        }

        /// <summary>
        /// Add an account.
        /// </summary>
        /// <param name="account">Account to add.</param>
        /// <returns>Returns whether the account is successfully added.</returns>
        public bool AddAccount(Account account)
        {
            return context.AddAccount(account);
        }

        /// <summary>
        /// Update account.
        /// </summary>
        /// <param name="account">Account to update.</param>
        /// <returns>Whether the account is successfully updated.</returns>
        public bool UpdateAccount(Account account)
        {
            return context.UpdateAccount(account);
        }

        /// <summary>
        /// Gets the account attached to the email.
        /// </summary>
        /// <param name="email">Email to check.</param>
        /// <returns>The account.</returns>
        public Account getAccount(string email)
        {
            List<Account> accounts = GetAll();
            foreach (Account account in accounts)
            {
                if (account.Email == email)
                {
                    return account;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the account by id.
        /// </summary>
        /// <param name="id">Id to search.</param>
        /// <returns>The account.</returns>
        public Account getAccount(int id)
        {
            List<Account> accounts = GetAll();
            foreach (Account account in accounts)
            {
                if (account.Id == id)
                {
                    return account;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks whether an email belongs to an admin.
        /// </summary>
        /// <param name="email">Email to find.</param>
        /// <returns>Whether the email belongs to an admin.</returns>
        public bool isAdmin(string email)
        {
            Account account = getAccount(email);
            if (account != null && account.isAdmin)
            {
                return true;
            }

            return false;
        }

        public bool IsAccountValid(Account account)
        {
            return context.IsAccountValid(account);
        }

        /// <summary>
        /// Checks whether the account exists.
        /// </summary>
        /// <param name="account">Account to check.</param>
        /// <returns>Whether the account exists.</returns>
        public bool AccountExists(Account account)
        {
            return context.AccountExists(account);
        }

        public bool EmailTaken(string email)
        {
            return context.EmailTaken(email);
        }
    }
}