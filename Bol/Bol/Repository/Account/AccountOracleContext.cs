﻿using System;
using System.Collections.Generic;
using System.Data;
using Bol.Models;
using Oracle.ManagedDataAccess.Client;

namespace Bol.Repository
{
    public class AccountOracleContext : IAccountContext
    {
        private OracleDatabaseHandler databaseHandler;

        public AccountOracleContext()
        {
            databaseHandler = new OracleDatabaseHandler();
        }

        /// <summary>
        /// Checks whether the account exists.
        /// </summary>
        /// <param name="account">Account to check.</param>
        /// <returns>Whether the account exists.</returns>
        public bool AccountExists(Account account)
        {
            List<Account> accounts = new List<Account>();

            DataTable dt = databaseHandler.ReadQuery("select * from gebruiker where lower(email) = lower('" + account.Email + "')");
            if (dt.Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Add an account.
        /// </summary>
        /// <param name="account">Account to add.</param>
        /// <returns>Whether the account is successfully added.</returns>
        public bool AddAccount(Account account)
        {
            string query = "INSERT INTO gebruiker(email, naam, wachtwoord, geboortedatum, telefoonnummer, admin)VALUES(:email, :name, :pass, :dob, :phone, :admin)";

            OracleCommand command = new OracleCommand();
            command.CommandText = query;
            command.Parameters.Add("email", OracleDbType.Varchar2, account.Email, ParameterDirection.Input);
            command.Parameters.Add("name", OracleDbType.Varchar2, account.Name, ParameterDirection.Input);
            command.Parameters.Add("pass", OracleDbType.Varchar2, account.Password, ParameterDirection.Input);
            if (account.DateOfBirth == DateTime.MinValue)
            {
                command.Parameters.Add("dob", OracleDbType.Date, DBNull.Value, ParameterDirection.Input);
            }
            else
            {
                command.Parameters.Add("dob", OracleDbType.Date, account.DateOfBirth, ParameterDirection.Input);
            }

            command.Parameters.Add("phone", OracleDbType.Varchar2, account.PhoneNumber, ParameterDirection.Input);
            command.Parameters.Add("admin", OracleDbType.Int32, (account.isAdmin ? 1 : 0), ParameterDirection.Input);

            return databaseHandler.ExecuteQuery(command);
        }

        /// <summary>
        /// Update account.
        /// </summary>
        /// <param name="account">Account to update.</param>
        /// <returns>Whether the account is updated.</returns>
        public bool UpdateAccount(Account account)
        {
            string query =
                "UPDATE gebruiker SET " +
                "email                   = :email,  " +
                "naam                   = :name,    " +
                "wachtwoord             = :pass,    " +
                "geboortedatum          = :dob,     " +
                "telefoonnummer         = :phone,   " +
                "admin                  = :admin    " +
                "where id               = :id       ";

            OracleCommand command = new OracleCommand();
            command.CommandText = query;
            command.Parameters.Add("email", OracleDbType.Varchar2, account.Email, ParameterDirection.Input);
            command.Parameters.Add("name", OracleDbType.Varchar2, account.Name, ParameterDirection.Input);
            command.Parameters.Add("pass", OracleDbType.Varchar2, account.Password, ParameterDirection.Input);
            if (account.DateOfBirth == DateTime.MinValue)
            {
                command.Parameters.Add("dob", OracleDbType.Date, DBNull.Value, ParameterDirection.Input);
            }
            else
            {
                command.Parameters.Add("dob", OracleDbType.Date, account.DateOfBirth, ParameterDirection.Input);
            }

            command.Parameters.Add("phone", OracleDbType.Varchar2, account.PhoneNumber, ParameterDirection.Input);
            command.Parameters.Add("admin", OracleDbType.Int32, (account.isAdmin ? 1 : 0), ParameterDirection.Input);
            command.Parameters.Add("id", OracleDbType.Int32, account.Id, ParameterDirection.Input);

            return databaseHandler.ExecuteQuery(command);
        }

        public bool EmailTaken(string email)
        {
            foreach (Account account in GetAll())
            {
                if (account.Email == email)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets all accounts.
        /// </summary>
        /// <returns>All accounts.</returns>
        public List<Account> GetAll()
        {
            List<Account> accounts = new List<Account>();

            DataTable dt = databaseHandler.ReadQuery("select * from gebruiker");
            foreach (DataRow row in dt.Rows)
            {
                Account account = new Account(Convert.ToInt32(row["id"].ToString()), row["email"].ToString(), row["wachtwoord"].ToString());

                if (row["naam"] != null)
                {
                    account.Name = row["naam"].ToString();
                }

                string date = row["geboortedatum"].ToString();
                if (date != null && date != string.Empty)
                {
                    DateTime dateTime;
                    DateTime.TryParse(date, out dateTime);
                    account.DateOfBirth = dateTime;
                }

                if (row["telefoonnummer"] != null)
                {
                    account.PhoneNumber = row["telefoonnummer"].ToString();
                }

                account.isAdmin = (Convert.ToInt32(row["admin"].ToString()) == 1 ? true : false);

                accounts.Add(account);
            }

            return accounts;
        }

        public bool IsAccountValid(Account account)
        {
            DataTable dt = databaseHandler.ReadQuery(string.Format("select count(*) as count from gebruiker where email = '{0}' and wachtwoord = '{1}'", account.Email, account.Password));
            if (Convert.ToInt32(dt.Rows[0]["count"]) <= 0)
            {
                return false;
            }

            return true;
        }
    }
}