﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bol.Repository
{
    public class Settings
    {
        public static readonly string DatabaseSource = "localhost";
        public static readonly string DatabaseUserId = "bol";
        public static readonly string DatabasePassword = "bol";
        public static readonly string ConnectionString = string.Format("Data Source={0}; User Id={1}; Password={2}", DatabaseSource, DatabaseUserId, DatabasePassword);
    }
}