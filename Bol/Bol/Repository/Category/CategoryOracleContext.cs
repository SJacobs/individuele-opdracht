﻿using System;
using System.Collections.Generic;
using System.Data;
using Bol.Models;
using Oracle.ManagedDataAccess.Client;

namespace Bol.Repository
{
    public class CategoryOracleContext : ICategoryContext
    {
        private OracleDatabaseHandler databaseHandler = new OracleDatabaseHandler();

        public bool AddCategory(Category category)
        {
            string query;
            if (category.ParentCategoryId <= 0)
            {
                query = string.Format("INSERT INTO Categorie(naam)values('{0}')", category.Name);
            }
            else
            {
                query = string.Format("INSERT INTO Categorie(categorie_id, naam)values({0}, '{1}')", category.ParentCategoryId, category.Name);
            }

            return databaseHandler.ExecuteQuery(query);
        }

        public List<Category> GetAll()
        {
            List<Category> categories = new List<Category>();

            DataTable dt = databaseHandler.ReadQuery("select * from categorie");
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"].ToString());
                string name = row["naam"].ToString();
                string upperIdString = row["categorie_id"].ToString();

                Category category = new Category(id, name);

                if (!string.IsNullOrWhiteSpace(upperIdString))
                {
                    int upperId = Convert.ToInt32(upperIdString);
                    category.ParentCategoryId = upperId;
                }
                else
                {
                    category.ParentCategoryId = 0;
                }

                category.Products = GetProductsForCategory(category);
                category.ChildCategories = GetChildCategories(category);

                categories.Add(category);
            }

            return categories;
        }

        public List<Category> GetChildCategories(Category category)
        {
            List<Category> childCategories = new List<Category>();

            DataTable dt = databaseHandler.ReadQuery(string.Format("select * from categorie where categorie_id = {0}", category.Id));
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"].ToString());
                string name = row["naam"].ToString();
                string upperIdString = row["categorie_id"].ToString();

                Category childCategory = new Category(id, name);

                if (!string.IsNullOrWhiteSpace(upperIdString))
                {
                    int upperId = Convert.ToInt32(upperIdString);
                    childCategory.ParentCategoryId = upperId;
                }

                childCategory.Products = GetProductsForCategory(childCategory);
                childCategory.ChildCategories = GetChildCategories(childCategory);
                childCategories.Add(childCategory);
            }

            return childCategories;
        }

        public Category GetCategory(int categoryId)
        {
            DataTable dt = databaseHandler.ReadQuery(string.Format("select * from categorie where id = {0}", categoryId));
            DataRow row = dt.Rows[0];

            int id = Convert.ToInt32(row["id"].ToString());
            string name = row["naam"].ToString();
            string upperIdString = row["categorie_id"].ToString();

            Category category = new Category(id, name);

            if (!string.IsNullOrWhiteSpace(upperIdString))
            {
                int upperId = Convert.ToInt32(upperIdString);
                category.ParentCategoryId = upperId;
            }

            category.Products = GetProductsForCategory(category);

            return category;
        }

        public List<Product> GetProductsForCategory(Category category)
        {
            List<Product> products = new List<Product>();

            DataTable dt = databaseHandler.ReadQuery(string.Format("select * from product where categorie_id = {0}", category.Id));
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"].ToString());
                int categoryId = Convert.ToInt32(row["categorie_id"].ToString());
                string name = row["naam"].ToString();
                decimal price = Convert.ToDecimal(row["prijs"].ToString());
                string brand = row["merk"].ToString();
                string type = row["type"].ToString();
                bool used = (Convert.ToInt32(row["gebruikt"].ToString()) == 1 ? true : false);
                string properties = row["eigenschappen"].ToString();
                string description = row["beschrijving"].ToString();

                Product product = new Product(id, categoryId, name, price, brand, type, used, properties, description);
                products.Add(product);
            }

            return products;
        }

        public bool UpdateCategory(Category category)
        {
            string query =
                "UPDATE category SET " +
                "naam           = :0," +
                "categorie_id   = :1 " +
                "where id       = :2";

            OracleCommand command = new OracleCommand();
            command.CommandText = query;

            command.Parameters.Add(new OracleParameter("0", OracleDbType.Varchar2, category.Name, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("1", OracleDbType.Int32, category.ParentCategoryId, ParameterDirection.Input));
            command.Parameters.Add(new OracleParameter("1", OracleDbType.Int32, category.Id, ParameterDirection.Input));

            return databaseHandler.ExecuteQuery(command);
        }
    }
}