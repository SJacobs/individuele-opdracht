﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public interface ICategoryContext
    {
        List<Category> GetAll();

        Category GetCategory(int id);

        List<Product> GetProductsForCategory(Category category);

        bool AddCategory(Category category);

        bool UpdateCategory(Category category);

        List<Category> GetChildCategories(Category category);
    }
}