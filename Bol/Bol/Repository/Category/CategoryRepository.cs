﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public class CategoryRepository
    {
        private ICategoryContext context = new CategoryOracleContext();

        public List<Category> GetAll()
        {
            return context.GetAll();
        }

        public Category GetCategory(int id)
        {
            return context.GetCategory(id);
        }

        public bool UpdateCategory(Category category)
        {
            return context.UpdateCategory(category);
        }

        public bool AddCategory(Category category)
        {
            return context.AddCategory(category);
        }
    }
}