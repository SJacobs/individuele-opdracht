﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bol.Models;

namespace Bol.Repository
{
    public class WishlistRepository
    {
        private IWishlistContext context;

        public WishlistRepository()
        {
            context = new WishlistOracleContext();
        }

        public WishlistRepository(IWishlistContext context)
        {
            this.context = context;
        }

        public List<ProductWithAmount> GetProducts(Account account)
        {
            return context.GetProducts(account);
        }

        /// <summary>
        /// Add a product to wish list.
        /// </summary>
        /// <param name="account">The account to add a product to.</param>
        /// <param name="product">The product to add.</param>
        /// <returns>Returns if successful.</returns>
        public bool AddProduct(Account account, Product product)
        {
            return context.AddProduct(account, product);
        }

        /// <summary>
        /// Delete a product from wish list.
        /// </summary>
        /// <param name="account">The account to delete the product from.</param>
        /// <param name="product">The product to delete.</param>
        /// <returns>Returns if the product was deleted.</returns>
        public bool DeleteProduct(Account account, Product product)
        {
            return context.DeleteProduct(account, product);
        }
    }
}