﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public interface IWishlistContext
    {
        bool AddProduct(Account account, Product product);

        bool DeleteProduct(Account account, Product product);

        List<ProductWithAmount> GetProducts(Account account);
    }
}