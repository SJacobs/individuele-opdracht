﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository.ShoppingCart
{
    public class ShoppingCartRepository
    {
        private IShoppingCartContext context;

        public ShoppingCartRepository()
        {
            context = new ShoppingCartOracleContext();
        }

        public List<ProductWithAmount> GetAll(Account account)
        {
            return context.GetAll(account);
        }

        public bool AddProduct(int accountId, Product product)
        {
            return context.AddProduct(accountId, product);
        }

        public bool DeleteProduct(int accountId, Product product)
        {
            return context.DeleteProduct(accountId, product);
        }
    }
}