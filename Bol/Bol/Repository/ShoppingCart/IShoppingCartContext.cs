﻿using System.Collections.Generic;
using Bol.Models;

namespace Bol.Repository
{
    public interface IShoppingCartContext
    {
        List<ProductWithAmount> GetAll(Account account);

        bool AddProduct(int accountId, Product product);

        bool DeleteProduct(int accountId, Product product);
    }
}