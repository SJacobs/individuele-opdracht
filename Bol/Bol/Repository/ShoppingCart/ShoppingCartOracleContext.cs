﻿using System;
using System.Collections.Generic;
using System.Data;
using Bol.Models;
using Oracle.ManagedDataAccess.Client;

namespace Bol.Repository.ShoppingCart
{
    public class ShoppingCartOracleContext : IShoppingCartContext
    {
        private OracleDatabaseHandler databaseHandler = new OracleDatabaseHandler();

        public List<ProductWithAmount> GetAll(Account account)
        {
            List<ProductWithAmount> cartProducts = new List<ProductWithAmount>();
            string query = string.Format("select * from winkelwagenproduct where gebruiker_id = {0}", account.Id);
            DataTable dt = databaseHandler.ReadQuery(query);
            foreach (DataRow row in dt.Rows)
            {
                int productId = Convert.ToInt32(row["product_id"].ToString());
                string productQuery = string.Format("select * from product where id = {0}", productId);
                DataTable productDt = databaseHandler.ReadQuery(productQuery);

                DataRow productRow = productDt.Rows[0];
                int id = Convert.ToInt32(productRow["id"].ToString());
                int categoryId = Convert.ToInt32(productRow["categorie_id"].ToString());
                string name = productRow["naam"].ToString();
                decimal price = Convert.ToDecimal(productRow["prijs"].ToString());
                string brand = productRow["merk"].ToString();
                string type = productRow["type"].ToString();
                bool used = (Convert.ToInt32(productRow["gebruikt"].ToString()) == 1 ? true : false);
                string properties = productRow["eigenschappen"].ToString();
                string description = productRow["beschrijving"].ToString();

                Product product = new Product(id, categoryId, name, price, brand, type, used, properties, description);
                ProductWithAmount cartProduct = new ProductWithAmount(product, Convert.ToInt32(row["aantal"].ToString()));
                cartProducts.Add(cartProduct);
            }

            return cartProducts;
        }

        public bool AddProduct(int accountId, Product product)
        {
            string countQuery = string.Format("SELECT aantal FROM winkelwagenproduct WHERE gebruiker_id = {0} AND product_id = {1}", accountId, product.Id);
            DataTable dt = databaseHandler.ReadQuery(countQuery);
            int count = 0;
            if (dt.Rows.Count > 0)
            {
                count = Convert.ToInt32(dt.Rows[0]["aantal"].ToString());
            }

            string query;

            OracleCommand command = new OracleCommand();
            if (count <= 0)
            {
                query = "INSERT INTO winkelwagenproduct(gebruiker_id, product_id, aantal)VALUES(:0, :1, :2)";
                command.CommandText = query;
                command.Parameters.Add("0", OracleDbType.Int32, accountId, ParameterDirection.Input);
                command.Parameters.Add("1", OracleDbType.Int32, product.Id, ParameterDirection.Input);
                command.Parameters.Add("2", OracleDbType.Int32, 1, ParameterDirection.Input);
            }
            else
            {
                query = "UPDATE winkelwagenproduct SET aantal = :0 WHERE gebruiker_id = :1 AND product_id = :2";
                command.CommandText = query;
                command.Parameters.Add("0", OracleDbType.Int32, (count + 1), ParameterDirection.Input);
                command.Parameters.Add("1", OracleDbType.Int32, accountId, ParameterDirection.Input);
                command.Parameters.Add("2", OracleDbType.Int32, product.Id, ParameterDirection.Input);
            }

            return databaseHandler.ExecuteQuery(command);
        }

        public bool DeleteProduct(int accountId, Product product)
        {
            string countQuery = string.Format("SELECT aantal FROM winkelwagenproduct WHERE gebruiker_id = {0} AND product_id = {1}", accountId, product.Id);
            DataTable dt = databaseHandler.ReadQuery(countQuery);
            int count = 0;
            if (dt.Rows.Count > 0)
            {
                count = Convert.ToInt32(dt.Rows[0]["aantal"].ToString());
            }
            
            string query;

            OracleCommand command = new OracleCommand();
            if (count == 1)
            {
                query = "DELETE FROM winkelwagenproduct WHERE gebruiker_id = :0 AND product_id = :1";
                command.CommandText = query;
                command.Parameters.Add("0", OracleDbType.Int32, accountId, ParameterDirection.Input);
                command.Parameters.Add("1", OracleDbType.Int32, product.Id, ParameterDirection.Input);
            }
            else
            {
                query = "UPDATE winkelwagenproduct SET aantal = :0 WHERE gebruiker_id = :1 AND product_id = :2";
                command.CommandText = query;
                command.Parameters.Add("0", OracleDbType.Int32, (count - 1), ParameterDirection.Input);
                command.Parameters.Add("1", OracleDbType.Int32, accountId, ParameterDirection.Input);
                command.Parameters.Add("2", OracleDbType.Int32, product.Id, ParameterDirection.Input);
            }

            return databaseHandler.ExecuteQuery(command);
        }
    }
}